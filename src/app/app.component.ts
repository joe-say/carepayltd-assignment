import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { UserService } from './shared/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  error: string = '';

  isLoggedIn: boolean = false;

  loginForm: FormGroup = new FormGroup({
    phoneNumber: new FormControl('', [Validators.required, Validators.minLength(9)]),
    password: new FormControl('', Validators.required)
  });

  user: any;

  wallets: any = [];

  constructor(private userService: UserService) {}

  signIn() {
    if (this.loginForm.valid) {
      this.userService.signIn(this.loginForm.value).subscribe((res: any) => {
        this.userService.currentUser = res;

        this.isLoggedIn = true;

        // get wallets
        this.walletsList();
      }, (error: any) => {
        this.error = error.json().message;

        setTimeout(() => {
          this.error = null;
        }, 2000);
      });
    }
  }

  walletsList() {
    this.userService.wallets().subscribe((res: any) => {
      this.wallets = res;
    });
  }
}
