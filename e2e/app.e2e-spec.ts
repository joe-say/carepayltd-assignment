import { CarepayltdPage } from './app.po';

describe('carepayltd App', () => {
  let page: CarepayltdPage;

  beforeEach(() => {
    page = new CarepayltdPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
