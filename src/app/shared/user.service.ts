import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

    _currentUser: any;

    requestOptions: RequestOptions = new RequestOptions({
        headers: new Headers({ 'Accept': 'application/json', 'Content-Type': 'application/json' })
    });

    constructor(private http: Http) {}

    signIn(credentials: any) {
        return this.http.post('http://apiv1.test.carepool.co.ke/users/login', JSON.stringify(credentials), this.requestOptions)
            .map(res => res.json());
    }

    wallets() {
        this.requestOptions.headers.append('authorization', `Bearer ${localStorage.getItem('carepay_token')}`);
        return this.http.get(`http://apiv1.test.carepool.co.ke/users/${this.currentUser.phoneNumber}/programs`, this.requestOptions)
                .map(res => res.json());
    }

    isLoggedIn() {
        const token: any = localStorage.getItem('carepay_token');

        if (token !== null) {
            return true;
        }

        return false;
    }

    get currentUser() {
        return this._currentUser;
    }

    set currentUser(user: any) {
        localStorage.setItem('carepay_token', user.token);
        this._currentUser = user;
    }
}
