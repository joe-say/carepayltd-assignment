import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Component({
    moduleId: module.id,
    selector: 'app-form-error',
    template: `
        <span *ngIf="errorMessage !== null" class="error">{{ errorMessage }}</span>
    `
})

export class MyFormErrorComponent {

    @Input()
     control: string;

    @Input()
     form: FormGroup;

    constructor() { }

    get errorMessage() {

        const c: AbstractControl = this.form.controls[this.control];
        console.log(c);
        if (c !== undefined) {
            for (const propertyName in c.errors) {
                if (c.errors.hasOwnProperty(propertyName) && (c.touched || c.invalid)) {
                    return this.getValidatorErrorMessage(propertyName);
                }
            }
        }

        return null;
    }

    getValidatorErrorMessage(code: string, errors: any = null) {
        const config: any = { required: 'This field is required' };

        switch (code) {
            case 'minlength':
                config['minlength'] = 'This field must not be less than ' + errors[code].requiredLength + ' characters';
             break;
        }
        console.log(config[code]);
        return config[code];
    }
}
